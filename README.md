# clisp-algoritmos-centralidade-grafos

Pontifícia Universidade Católica do Paraná
Bacharelado em Ciência da Computação
Disciplina Programação Funcional - Prof. Fabrício Enembreck

Título do Trabalho:
    Algoritmos de Centralidade em Grafos

Requisitos de implementação:
    (2,0 Pontos) Módulo de carregamento e gravação de Grafo não direcionado, ponderado e rotulado em Arquivo (Formato Pajek em anexo): Construir um interpretador para um grafo Pajek de forma que o grafo seja devidamente criado em memória conforme visto em sala.
    Implementar em LISP:
        (2,0 Pontos) Função que calcula a Centralidade de Grau (Degree) de cada nó;
        (2,0 Pontos) Função que calcula a Centralidade de Proximidade (Closeness) de cada nó (considere a distância do melhor caminho e não o caminho geodésico);
        (2,0 Pontos) Função que calcula a Centralidade de Intermediação (Betweeness) de cada nó (considere a distância do melhor caminho e não o caminho geodésico);
        (2,0 Pontos) Apresentar uma tabela de scores para um grafo específico no seguinte formato:

Referência para o cálculo das medidas de centralidade (Degree, Closeness e Betweeness):
https://drive.google.com/file/d/0B_LQTp76u16ISVh3Ui1lSk9TVjg/view    pg 80 (também disponível no Eureka).

Formato Pajek:

*Vertices  8
1 "Actor 1"
2 "Actor 2"
3 "Actor 3"
4 "Event 1"
5 "Event 2"
6 "Event 3"
7 "Event 4"
8 "Event 5"

*Edges
1 4 10
1 5 4
2 4 3
2 5 5
2 6 8
2 8 12
3 4 11
3 7 2
3 8 7